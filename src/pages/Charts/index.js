import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { 
    Box,
    CssBaseline, 
    Container, 
    TextField,
    Grid,
    Typography,
    Paper,
    Divider,
    Snackbar,
    Tabs,
    Tab,
    AppBar
} from '@material-ui/core';

import {
    Radar, 
    RadarChart, 
    ResponsiveContainer,
    PolarGrid, 
    PolarAngleAxis, 
    PolarRadiusAxis,
    LineChart, 
    Line, 
    XAxis, 
    YAxis, 
    CartesianGrid, 
    Tooltip, 
    Legend,
    BarChart,
    Bar
} from 'recharts';

import MuiAlert from '@material-ui/lab/Alert';
import formStyles from './styles';
import { createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import config from '../../config/app.config';

const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography component={'span'}>{children}</Typography>
          </Box>
        )}
      </div>
    );
}

TabPanel.propTypes = {
children: PropTypes.node,
index: PropTypes.any.isRequired,
value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
;


const Chart = () => {
    
    const classes = formStyles();
    const [error, setError] = useState(false);
    const [value, setValue] = useState(0);
    const [x, setX] = useState({
        x0: 'a',
        x1: 'b',
        x2: 'c',
        x3: 'd',
        x4: 'e',
        x5: 'f'
    });
    const [y, setY] = useState({
        y0: '80',
        y1: '90',
        y2: '60',
        y3: '30',
        y4: '70',
        y5: '100'
    });

    const data = [
        {name: x.x0, pv: y.y0},
        {name: x.x1, pv: y.y1},
        {name: x.x2, pv: y.y2},
        {name: x.x3, pv: y.y3},
        {name: x.x4, pv: y.y4},
        {name: x.x5, pv: y.y5},
    ];    

    const dataRadar = [
        {subject: x.x0, A: y.y0, fullMark: 100},
        {subject: x.x1, A: y.y1, fullMark: 100},
        {subject: x.x2, A: y.y2, fullMark: 100},
        {subject: x.x3, A: y.y3, fullMark: 100},
        {subject: x.x4, A: y.y4, fullMark: 100},
        {subject: x.x5, A: y.y5, fullMark: 100},
    ];


    const handleError = (reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setError(false)
    }

    const handleUpdateX = (e) => {
        setX({
            ...x,
            [e.target.name]: e.target.value
        })
        console.log(x)
    }

    const handleUpdateY = (e) => {
        if(e.target.value > 100){
            setError(true)
            setY('')
        } else{
            setError(false)
            setY({
                ...y,
                [e.target.name]: e.target.value
            })
        }
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const theme = createMuiTheme({
        overrides: {
            MuiAppBar: {
               colorDefault: {
                   backgroundColor: '#800000',
               } 
            },
            MuiTab: {
                textColorInherit: {
                    color: '#fff'
                },
            },
        },
        // Typ
        typography: {
            fontFamily: [
                'Roboto',
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
        h2: {
            fontSize: '1.5rem'
        }},
        // Typ
    });

    return (
        <>
        <CssBaseline/>
            <ThemeProvider theme={theme}>
                <Container className={classes.container}>
                    
                <Paper elevation={1}> 
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <form action={`${config.api}/chart`} method="POST">
                            <Grid container className={classes.gridContainerTitle}>
                                <Grid item xs={12} className={classes.gridItemTitle}>
                                    <Typography component={'span'} variant="h2">
                                        Gráficos dinâmicos
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Divider variant="fullWidth" style={{marginBottom:'15px'}}/>
                            
                            <Grid container className={classes.gridContainerBody}>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x0)}
                                        onChange={handleUpdateX}
                                        label="x0"
                                        name='x0'
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y0)}
                                        onChange={handleUpdateY}
                                        label="y0"
                                        name='y0'
                                        required
                                        size='small'
                                        type='number'
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x1)}
                                        onChange={handleUpdateX}
                                        label="x1"
                                        name='x1'
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y1)}
                                        label="y1"
                                        required
                                        onChange={handleUpdateY}
                                        size='small'
                                        name='y1'
                                        type='number'
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x2)}
                                        onChange={handleUpdateX}
                                        label="x2"
                                        name='x2'
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y2)}
                                        label="y2"
                                        required
                                        onChange={handleUpdateY}
                                        size='small'
                                        name='y2'
                                        type='number'
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x3)}
                                        onChange={handleUpdateX}
                                        label="x3"
                                        name='x3'
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y3)}
                                        label="y3"
                                        required
                                        onChange={handleUpdateY}
                                        size='small'
                                        name='y3'
                                        type='number'
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x4)}
                                        onChange={handleUpdateX}
                                        label="x4"
                                        name="x4"
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y4)}
                                        label="y4"
                                        required
                                        onChange={handleUpdateY}
                                        size='small'
                                        name='y4'
                                        type='number'
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} className={classes.gridItemBody}>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={String(x.x5)}
                                        onChange={handleUpdateX}
                                        label="x5"
                                        name='x5'
                                        required
                                        size='small'
                                        />
                                    </Grid>
                                    <Grid item sm={3}>
                                        <TextField
                                        variant="outlined"
                                        value={Number(y.y5)}
                                        label="y5"
                                        required
                                        onChange={handleUpdateY}
                                        size='small'
                                        type='number'
                                        name='y5'
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </form>
                    </div>                                            

                    <div style={{padding: '10px', }}>
                        <AppBar position="static" color="default" style={{borderRadius: '5px'}} >
                            <Tabs value={value} onChange={handleChange} variant="fullWidth" indicatorColor="secondary" >
                            <Tab label="Radar" {...a11yProps(0)} />
                            <Tab label="Line" {...a11yProps(1)} />
                            <Tab label="Bar" {...a11yProps(2)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={value} index={0}>
                            <div style={{display: 'flex', justifyContent: 'center'}}>
                                <Grid container className={classes.gridContainerLine}> 
                                    <Grid item xs={12} className={classes.gridItemAreaRadar}>
                                        <ResponsiveContainer width="90%">
                                            <RadarChart 
                                                outerRadius={60} 
                                                data={dataRadar}
                                            >
                                                <PolarGrid />
                                                <PolarAngleAxis dataKey="subject" />
                                                <PolarRadiusAxis />
                                                <Radar dataKey='A' stroke="#443dc2" fill="#8884d8" fillOpacity={0.4} />
                                            </RadarChart>
                                        </ResponsiveContainer>
                                    </Grid>
                                </Grid>
                            </div>
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <div style={{display: 'flex', justifyContent: 'center'}}>
                                <Grid container className={classes.gridContainerLine}>
                                    <Grid item xs={12} >
                                        <ResponsiveContainer margin='0' width="95%" height="90%" >
                                            <LineChart
                                                data={data}
                                            >
                                                <CartesianGrid strokeDasharray="2 2" />
                                                <XAxis dataKey="name" />
                                                <YAxis />
                                                <Tooltip />
                                                <Legend />
                                                <Line type="monotone" dataKey="pv" stroke="#8884d8" activeDot={{r: 8}} />
                                            </LineChart>
                                        </ResponsiveContainer>  
                                    </Grid>
                                </Grid>
                            </div>
                        </TabPanel>
                        <TabPanel value={value} index={2}>
                            <div style={{display: 'flex', justifyContent: 'center'}}>
                                <Grid container className={classes.gridContainerLine}>
                                    <Grid item xs={12} >
                                        <ResponsiveContainer margin='0' width="95%" height="90%" >
                                            <BarChart width={730} height={250} data={data}>
                                                <CartesianGrid strokeDasharray="3 3" />
                                                <XAxis dataKey="name" />
                                                <YAxis />
                                                <Tooltip />
                                                <Legend />
                                                <Bar dataKey="pv" fill="#8884d8" />
                                            </BarChart>
                                        </ResponsiveContainer>  
                                    </Grid>
                                </Grid>
                            </div>
                        </TabPanel>
                    </div>

                    


                    

                    

                    </Paper>
                    <Snackbar open={error} autoHideDuration={2000} onClose={handleError}>
                        <Alert onClose={handleError} severity="error">
                            Número deve ser no máximo 100.
                        </Alert>
                    </Snackbar>

                </Container>
            </ThemeProvider>
        </>
    )
}

export default Chart;