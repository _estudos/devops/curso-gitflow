import { makeStyles } from '@material-ui/core/styles';


const formStyles = makeStyles((theme) => ({
    // BASE
    container: {
      display: "flex",
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '10px',
      marginTop: '15px',
      width: '100%'
    },
    paper: {
      borderRadius: '15px',
      padding: '15px',
      width: '100%'
    },
      
    // TITULO
    gridContainerTitle: {
      padding: '10px',
    },
    gridItemTitle: {
      display: 'flex',
      justifyContent: 'center',
      margin: '10px',
      maxWidth: '95%',
    },
      
    // INPUTS
    gridContainerBody: {
      display: 'flex',
      justifyContent: 'center',
      padding: '5px',
    },
    gridItemBody: {
      display: 'flex',
      justifyContent: 'center',
      padding: '15px',
    },
      

  // GRAFICO RADAR
    gridItemTypoRadar: {
      display: 'flex',
      justifyContent: 'center',
    },
    gridItemAreaRadar: {
      display: 'flex',
      justifyContent: 'center',
      height: '260px',
      width: '80vmin',
      padding: '15px'
    },

    // GRAFICO LINHA
    gridContainerLine: {
      display: 'flex',
      height: '280px',
      width: '60vmin',
      padding: '5px',
      borderRadius: '5px',

    },
    gridItemTypoLinha: {
      display: 'flex',
      justifyContent: 'center',
    },
    gridItemAreaLinha: {
      display: 'flex',
      justifyContent: 'flex-start',
      width: '100%',
      background: 'red'
    },
    bar: {
      display: 'flex',
      textAlign: 'center'
    }

}));

export default formStyles;