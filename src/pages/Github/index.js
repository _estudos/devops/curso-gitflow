import React, { useState } from 'react';
import axios from 'axios';
import { 
  Link, 
  Typography, 
  Container,
  Grid, 
  TextField, 
  Snackbar,
  CssBaseline,
  InputAdornment,
  Divider
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import GitHubIcon from '@material-ui/icons/GitHub';
import globalStyles from '../../styles/index';
import { createMuiTheme , ThemeProvider } from '@material-ui/core/styles';

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Github = () => {

  // Altera tema MaterialUI
  const theme = createMuiTheme({
    typography: {
      fontFamily: [
        'Roboto',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
    ].join(','),
    subtitle1: {
      fontSize: '0.5rem'
    },
    h2: {
      fontSize: '2rem'
    },
    h3: {
      fontSize: '1.5rem'
    },
    fontSize: 12
  },
  });

  const classes = globalStyles();
  const [data, setData] = useState([]);
  const [name, setName] = useState([]);
  const [error, setError] = useState(false);
  const url = `https://api.github.com/users/${name}`;
  // const proxyUrl = 'https://cors-anywhere.herokuapp.com/';

  const handleSubmit = (evt) => {
    evt.preventDefault()
    getUrl()
  }

  const handleError = (reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setError(false)
  }
  
  var getUrl = () => {
    axios.get(url)
      .then((res) => {
          const {data} = res;
          var reg = /\D+/;
          var datas = data.created_at.split(reg);
          var diaMesAno = `${datas[2]}/${datas[1]}/${datas[0]}`
          setData({...data, diaMesAno});
          setError(false);
      })
      .catch(err => {
        console.log(err);
        setError(true);
      })
  }

  return(
  <>
    <CssBaseline/>
      <ThemeProvider theme={theme}>
        <Container className={classes.container}>

          <form onSubmit={handleSubmit}>

            <Grid container className={classes.gridTitle}>
              <Grid item xs={12}>
                <Typography variant="h2" className={classes.gridTitleTypo}>
                    Consulta GitHub
                </Typography>
              </Grid>
              <Grid item xs={12} className={classes.gridTitleField}>
                <TextField 
                    type="text" 
                    label="Digite o usuário"
                    size="medium" 
                    variant="outlined" 
                    value={name} 
                    onChange={e => setName(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          <GitHubIcon/>
                        </InputAdornment>
                      ),
                    }}
                />
              </Grid>              
            </Grid>
            
            <Divider style={{marginBottom: '15px'}}/>

            <Grid container className={classes.containerData}>

              <Grid item xs={12}>
                <img alt="" src={data.avatar_url} target="blank" className={classes.img}/>
              </Grid>

              <Grid item xs={12} className={classes.gridData}>
                <Typography variant="h3" className={classes.gridTitleTypo}>
                   {(data.name === null || data.name === undefined) ? '' : <strong>{data.name}</strong>}
                </Typography>
              </Grid>

              <Grid item xs={12} className={classes.gridData}>
                {(data.location === null || data.location === undefined) ? '' : data.location}
              </Grid>

              <Grid item xs={12} className={classes.gridData}>
                {(data.bio === null || data.bio === undefined) ? '' : data.bio}
              </Grid>
           
              <Grid item xs={12} className={classes.gridData}>
                {(data.public_repos === null || data.public_repos === undefined) ? '' : 'Repos: '}
                {(data.public_repos === null || data.public_repos === undefined) ? '' : data.public_repos}
              </Grid>

              <Grid item xs={12} className={classes.gridData}>
                <Link target="_blank" href={data.html_url}> {data.html_url}</Link>
              </Grid>
            </Grid>
          </form>

        <Snackbar open={error} autoHideDuration={2000} onClose={handleError}>
            <Alert onClose={handleError} severity="error">
              Digite um usuário válido.
            </Alert>
          </Snackbar>
          
        </Container>
    </ThemeProvider>
  </>
  )
}


export default Github;