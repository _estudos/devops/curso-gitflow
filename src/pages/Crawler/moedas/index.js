import React, {useState, useEffect} from 'react';
import { 
    Container,
    CssBaseline,
    Divider, 
    Grid,
    Snackbar,
    Table,
    TableContainer,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Typography,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import globalStyles from '../../../styles/index';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const MOEDAS = () => {

    // Altera tema MaterialUI
    const theme = createMuiTheme({
        typography: {
        fontFamily: [
            'Roboto',
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        subtitle1: {
        fontSize: '0.5rem'
        },
        h2: {
        fontSize: '2rem'
        },
        h3: {
        fontSize: '1.5rem'
        },
        fontSize: 12
    },
    });

    const classes = globalStyles();
    const [data, setData] = useState([]);
    const [snack, setSnack] = useState(false);
    const [progress, setProgress] = useState('')
    
    const handleError = (reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack(false)
    }

    useEffect(() => {
        (async () => {
            setProgress(<CircularProgress />)
            const res = await fetch(`http://192.168.0.248:3001/moedas`)
            if(res.status === 200){
               const json = await res.json()
               if(json.message) {
                setProgress('')
                const message = json.message
                console.log({message})
            } else {
                   setProgress('')
                   setData(json)
               }
    
            }  
        })()
      }, [])

    return(
        <>
        <CssBaseline/>
            <ThemeProvider theme={theme}>
                <Container className={classes.container}>

                    <form 
                        action={`http://192.168.0.248:3001/moedas/`}
                        method="GET"
                    >
                        <Grid container spacing={0} className={classes.gridTitle}>
                            <Grid item xs={2}>
                                <MonetizationOnIcon style={{fontSize: 50}}/>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="h2">
                                    Cotação de moedas
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="subtitle2">
                                    Valor referente a R$ 1,00.
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="subtitle2">
                                    <em><a href="https://www.infomoney.com.br/" target="blank">Fonte</a></em>
                                </Typography>
                            </Grid>
                        </Grid>

                        <Divider style={{margin:'15px'}} />
                        
                        <div style={{display: 'flex', justifyContent:'center'}}>
                            {progress}
                        </div>
                        <TableContainer style={{marginBottom: '40px'}}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell><strong>Moeda</strong></TableCell>
                                        <TableCell align="right"><strong>Venda</strong></TableCell>
                                        <TableCell align="right"><strong>Compra</strong></TableCell>
                                        <TableCell align="right"><strong>Var.</strong></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow key={data.nomePeso}>
                                        <TableCell component="th" scope="row">
                                            {data.nomePeso}
                                        </TableCell>
                                        <TableCell align="right">{data.pesoCompra}</TableCell>
                                        <TableCell align="right">{data.pesoVenda}</TableCell>
                                        <TableCell align="right" style={(parseFloat(data.pesoVar) > 0) ? {color:'lightgreen'} : {color:'red'}}>
                                            {data.pesoVar}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow key={data.nomeDolarCom}>
                                        <TableCell component="th" scope="row">
                                            {data.nomeDolarCom}
                                        </TableCell>
                                        <TableCell align="right">{data.dolarComCompra}</TableCell>
                                        <TableCell align="right">{data.dolarComVenda}</TableCell>
                                        <TableCell align="right" style={(parseFloat(data.dolarComVar) > 0) ? {color:'lightgreen'} : {color:'red'}}>
                                            {data.dolarComVar}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow key={data.nomeDolarTur}>
                                        <TableCell component="th" scope="row">
                                            {data.nomeDolarTur}
                                        </TableCell>
                                        <TableCell align="right">{data.dolarTurCompra}</TableCell>
                                        <TableCell align="right">{data.dolarTurVenda}</TableCell>
                                        <TableCell align="right" style={(parseFloat(data.dolarTurVar) > 0) ? {color:'lightgreen'} : {color:'red'}}>
                                            {data.dolarTurVar}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow key={data.nomeEuro}>
                                        <TableCell component="th" scope="row">
                                            {data.nomeEuro}
                                        </TableCell>
                                        <TableCell align="right">{data.euroCompra}</TableCell>
                                        <TableCell align="right">{data.euroVenda}</TableCell>
                                        <TableCell align="right" style={(parseFloat(data.euroVar) > 0) ? {color:'lightgreen'} : {color:'red'}}>
                                            {data.euroVar}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                </form>
                        

                <Snackbar open={snack} autoHideDuration={3000} onClose={handleError}>
                    <Alert onClose={handleError} severity="error">
                        Não foi possível encontrar os valores. 
                    </Alert>
                </Snackbar>
            </Container>
        </ThemeProvider>
        </>
    )
}

export default MOEDAS