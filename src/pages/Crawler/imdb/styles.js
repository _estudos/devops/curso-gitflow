import { makeStyles } from '@material-ui/core/styles';


const crawlerStyles = makeStyles(() => ({
    root: {
      maxWidth: '350px',
      float: 'left',
      marginBottom: '15px',
      marginRight: '15px',
    },
    img: {
      margin: '0 15px',
      maxWidth: "85%",
      height: "auto",
    },
    gfg: { 
      width:"auto",
    },
    progress: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '50px'
    },
    input: {
      display: 'flex',
      flexGrow: 4,
      flexShrink: 3,
    },
    icone: {
      display: 'flex',
      flexGrow: 1,
      color: "#fff", 
      flexShrink: 3
    }

  
}));

export default crawlerStyles;