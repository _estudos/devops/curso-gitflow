import React, {useState} from 'react';

import { 
    CssBaseline,
    Card, 
    CardContent, 
    CardHeader, 
    Divider, 
    Fab, 
    TextField,
    Typography,
    Snackbar,
    Grid,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import SearchIcon from '@material-ui/icons/Search';
import { FaImdb } from 'react-icons/fa';
import crawlerStyles from './styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const IMDB = () => {

    const classes = crawlerStyles();
    const [data, setData] = useState([]);
    const [name, setName] = useState([]);
    const [progress, setProgress] = useState('')
    const [snack, setSnack] = useState(false);
    const [notFind, setNotFind] = useState(false);
    
    const handleSubmit = (evt) => {
        setProgress(<CircularProgress />)
        evt.preventDefault()
        setData('')
        fetchURL()
    }

    const handleError = (reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnack(false)
        setNotFind(false)
    }

    const fetchURL = async () => {
        const res = await fetch(`http://192.168.0.248:3001/imdb/${name}`)
        
        if(res.status === 200){
           setProgress(progress)
           const json = await res.json()
           if(json.message) {
               const message = json.message
               setData('')
               setProgress('')
               setSnack(false)
               setNotFind(true)
               console.log({message})
           } else {
               setData('')
               setProgress('')
               setNotFind(false)
               setSnack(false)
               setData(json)
           }

        }  else {
            setData('')
            setProgress('')
            setNotFind(false)
            setSnack(true)
        }
    }


    return(
        <>
        <CssBaseline/>
        <div className={classes.root}>
            <form 
                action={`http://192.168.0.248:3001/imdb/`}
                method="GET"
                >
                    <div className={classes.gfg}>

                        <Card>
                        <CardHeader
                            style={{textAlign:'center'}}
                            avatar={
                                <FaImdb size={50}/>
                            }
                        />
                        <CardContent>
                            <Grid container spacing={2} className={classes.input}>
                                <Grid item xs={9}>
                                        <TextField 
                                            type="text" 
                                            variant="outlined"
                                            id="outlined-basic" 
                                            label="Buscar" 
                                            size="small" 
                                            value={name} 
                                            onChange={e => setName(e.target.value)}
                                            placeholder="Insira o título desejado"
                                            />
                                        </Grid>
                                        <Grid item xs={3}>
                                        <Fab
                                            size="small"
                                            type="submit"
                                            style={{ background: "#003366", marginLeft: "5px"}}
                                            onClick={handleSubmit}
                                            className={classes.icone}
                                            >
                                            <SearchIcon className={classes.icone}/>
                                        </Fab>
                                </Grid>
                            </Grid>
                        </CardContent>

                        <Divider style={{margin:'15px'}}/>
                        
                        <div className={classes.progress}>
                            {progress}

                        </div>
                        <div>
                            <img alt="" src={data.url} className={classes.img}/>
                        </div>
                        <div className={classes.root}>
                            <CardContent>
                                <Typography paragraph >
                                    {<em>{data.titulo}</em>} 
                                </Typography>
                                <Typography paragraph >
                                    {data.original} 
                                </Typography>
                                <Typography paragraph >
                                    {(data.classificacao === undefined) ? data.classificacao : `Classificação: ${data.classificacao}`}
                                </Typography>
                                <Typography paragraph >
                                  {(data.resumo === undefined) ? data.resumo : `Resumo: ${data.resumo}`}
                                </Typography>
                            </CardContent>
                        </div>
                        </Card>
                    </div>
            </form>
        </div>

        <Snackbar open={snack} autoHideDuration={3000} onClose={handleError}>
            <Alert onClose={handleError} severity="error">
                Digite um título válido.
            </Alert>
        </Snackbar>
        <Snackbar open={notFind} autoHideDuration={3000} onClose={handleError}>
            <Alert onClose={handleError} severity="error">
                Título não encontrado.
            </Alert>
        </Snackbar>
        </>
    )
}

export default IMDB