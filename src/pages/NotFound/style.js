import { makeStyles } from '@material-ui/core/styles';

const useStyleNotFound = makeStyles(theme => ({
    root: {
      padding: theme.spacing(4),
    },
    content: {
      paddingTop: 5,
      textAlign: 'center'
    },
    image: {
      marginTop: 50,
      display: 'inline-block',
      maxWidth: '100%',
      width: 560
    }
  }));

  export default useStyleNotFound