import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import useStyleNotFound from './style'
import notfound from '../../assets/images/undraw_page_not_found_su7k.svg';
import { Link } from 'react-router-dom';


const NotFound = () => {
  const classes = useStyleNotFound();

  return (
    <>
    <div className={classes.root}>
      <Grid
        container
        justify="center"
        spacing={4}
      >
        <Grid
          item
          lg={9}
          xs={12}
        >
          <div className={classes.content}>
            <Typography variant="h2">
              A página que você está procurando não existe
            </Typography>
            <Typography variant="subtitle2">
                Você tentou alguma rota oculta ou veio aqui por engano.
                Seja o que for, tente usar a navegação por <Link to="/">links</Link>.
            </Typography>
            <img
              src={notfound}
              alt="Acesso ilegal"
              className={classes.image}
            />
          </div>
        </Grid>
      </Grid>
    </div>
    </>
  );
};

export default NotFound;