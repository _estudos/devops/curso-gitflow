
  import { makeStyles} from '@material-ui/core/styles'

  const chatStyles = makeStyles((theme) => ({
    paper: {
        display: 'flex',
        justifyContent: 'spaceBetween',
        flexDirection: 'column',
        height: '90vmin',
        width: '85vmin',
    },
    list: {
        margin: 0,
        paddind: '1rem',
    },
    list_item: {
        listStyle: 'none'
    },
    list__item__meu: {
        listStyle: 'none',
        textAlign: 'right'
    },
    messageMeu: {
        border: '1px solid transparent',
        borderRadius: '3px',
        display: 'inlineBlock',
        listStyle: 'none',
        marginBottom: '1rem',
        padding: '.5rem 1rem',
        background: "#c3e88d",
        borderColor: "#82be27",
        textAlign: 'right'
    },
    messageOutros: {
        border: '1px solid transparent',
        borderRadius: '5px',
        display: 'inlineBlock',
        listStyle: 'none',
        marginBottom: '1rem',
        padding: '.5rem 1rem',
        background: '#89ddff',
        borderColor: "#1abeff"
    },
    form: {
        padding: '1rem',
        background: 'lightgray'
        
    },
    input: {
        borderRadius: '15px',
        padding: '1rem'
    }



}));

export default chatStyles