// import React, { useState , useEffect} from 'react';
// import io from 'socket.io-client';
// import {v4 as uuidv4} from 'uuid';

// import {
//     CssBaseline,
//     Paper,
// }from '@material-ui/core';
// import chatSyles from './styles';

// const socket = io('http://192.168.0.248:3002');
// const myId = uuidv4();
// socket.on('connect', () => console.log('[IO] Connect => A new Connection has been established'))


// const Chat = () => {

//     const classes = chatSyles();
//     const [ message, setMessage ] = useState('');
//     const [ messages, setMessages ] = useState([]);

//     // Primeiro parametro (Função) - Segundo (Array recebe as propriedades a serem ouvidas as mudanças - Caso vazio, dispara apenas uma vez)
//     useEffect(() => {
//         const handleNewMessage = newMessage => 
//             setMessages([...messages, newMessage])
        
//         socket.on('chat.message', handleNewMessage)
//         return () => socket.off('chat.message', handleNewMessage)
//         // eslint-disable-next-line
//     }, [messages])

    
//     const handleInputChange = evt => {
//         setMessage(evt.target.value)
//     }

//     const handleFormSubmit = evt => {
//         evt.preventDefault();

//         // Trim testa se input esta vázio removendo os espaços. Se true, entra na condição do if
//         if(message.trim()){
//             socket.emit('chat.message', {
//                 id: myId,
//                 message
//             })
//             setMessage('')
//         }
//     }

//     return(
//          <>
//             <CssBaseline />
//                 <Paper className={classes.paper}>
//                     <ul>
//                         {messages.map((m, index)=> (
//                             <li className={(m.id === myId) ? classes.list__item__meu : classes.list_item}  key={index}>
//                                 <span className={(m.id === myId) ? classes.messageMeu : classes.messageOutros}>
//                                     {m.message}
//                                 </span>
//                             </li>
//                         ))}
                        
//                     </ul>
//                     <form className={classes.form} onSubmit={handleFormSubmit}>
//                         <input 
//                             className={classes.input}
//                             placeholder="Escreva sua mensagem"
//                             onChange={handleInputChange}
//                             value={message}
//                         />
//                     </form>
//                 </Paper>
//          </>
//     )
// }

// export default Chat;