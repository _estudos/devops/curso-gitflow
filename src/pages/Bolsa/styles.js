import { makeStyles} from '@material-ui/core/styles';
require('typeface-roboto')

const bolsaStyles = makeStyles(theme => ({
    
    root: {
        minWidth: '100%',
        fontFamily: theme.typography.fontFamily,
    },
    cardContainer: {
        background: 'lightblue',
    },
}));

export default bolsaStyles;