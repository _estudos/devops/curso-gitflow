import React, {useState} from 'react';
import axios from 'axios';

import { 
  Container,
  InputAdornment,
  Grid,
  Typography,
  Divider,
  TextField,
  Snackbar,
  CssBaseline,
} from '@material-ui/core';

import MuiAlert from '@material-ui/lab/Alert';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import globalStyles from '../../styles/index';

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Bolsa = () => {
    
   // Altera tema MaterialUI
  const theme = createMuiTheme({
    typography: {
      fontFamily: [
        'Roboto',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
    ].join(','),
    h2: {
      fontSize: '2rem'
    },
    fontSize:12
  },
  });

    const classes = globalStyles();

    var Ativo = ''
    const [data, setData] = useState([]);
    const [dataName, setDataName] = useState([]);
    const [name, setName] = useState([]);
    const [error, setError] = useState(false);
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/'
    const url = "http://cotacoes.economia.uol.com.br/ws/asset/stock/list?size=10000"
    
    const handleSubmit = (evt) => {
        evt.preventDefault();
        fetchUrl()
    };

    const handleError = (reason) => {
      if (reason === 'clickaway') {
        return;
      }
      setError(false)
    }
    
    const fetchUrl = () => {
      axios.get(proxyUrl+url)
        .then(res => {
          const { data } = res.data;
          return data;
        })
        .then((arrData) => {
          arrData.forEach(item => {
            if(item.code === `${name.toUpperCase()}.SA`){
              Ativo = item.idt
              setDataName(item)
            }
          })
          return Ativo
        })
        .then(ativo => {
          const intraUrl = `http://cotacoes.economia.uol.com.br/ws/asset/${ativo}/intraday?size=400`;
            axios.get(proxyUrl+intraUrl)
            .then(res => {
              const { data } = res.data;
              if(data === undefined){
                setError(true);
              } else {
                setError(false);
                setData(data[0])
                }
            })
          })
    }
        
    return (
      <>
      <CssBaseline/>
        <ThemeProvider theme={theme}>
          <Container className={classes.container}>
  
            <form onSubmit={handleSubmit}>
  
              <Grid container className={classes.gridTitle}>
                <Grid item xs={12} >
                  <Typography variant="h2" className={classes.gridTitleTypo} >
                      Consulta bolsa de valores
                  </Typography>
                </Grid>
                <Grid item xs={12} >
                  <Typography variant="subtitle1" className={classes.gridTitleTypo}>
                      Pesquise pelo ativo desejado. 
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" className={classes.gridTitleTypo}>
                      <a href='http://cotacoes.economia.uol.com.br/acoes-bovespa.html' target="blank"> Lista de referência</a>
                  </Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridTitleField}>
                  <TextField 
                      type="text" 
                      label="Digite aqui"
                      size="medium" 
                      variant="outlined" 
                      value={name} 
                      onChange={e => setName(e.target.value)}
                      placeholder='Ex. petr4, elet3'
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="start">
                            <AttachMoneyIcon/>
                          </InputAdornment>
                        ),
                      }}
                  />
                </Grid>              
              </Grid>
              
              <Divider style={{marginBottom: '15px'}}/>

              <Grid container className={classes.containerData}>

                <Grid item xs={12} className={classes.gridData}>
                  <Typography variant="h3" className={classes.gridTitleTypo}>
                    {(dataName.name === undefined || dataName.name === null) ? '' : `${dataName.name}`}
                  </Typography>
                </Grid>
  
                <Grid item xs={12} className={classes.gridData}>
                  {(dataName.code === undefined || dataName.code === null) ? '' : 'Código: '}
                  {(dataName.code === undefined || dataName.code === null) ? '' : `${dataName.code}`}
                </Grid>
  
                <Grid item xs={12} className={classes.gridData}>
                  {(data.price === undefined || data.price === null) ? '' : `Cotação: `}
                  {(data.price === undefined || data.price === null) ? '' : `${data.price}`}
                </Grid>

                <Grid item xs={12} className={classes.gridData}>
                  {(data.vol === undefined || data.vol === null) ? '' : `Volume: `}
                  {(data.vol === undefined || data.vol === null) ? '' : `${data.vol}`}
                </Grid>

                <Grid item xs={12} className={classes.gridData}>
                  {(data.high === undefined || data.high === null) ? '' : `Máximo (dia): `}
                  {(data.high === undefined || data.high === null) ? '' : `${data.high}`}
                </Grid>

                <Grid item xs={12} className={classes.gridData}>
                  {(data.low === undefined || data.low === null) ? '' : `Mínimo (dia): `}
                  {(data.low === undefined || data.low === null) ? '' : `${data.low}`}
                </Grid>

                <Grid item xs={12} className={classes.gridData}>
                  {(data.varpct === undefined || data.varpct === null) ? '' : `Variação: `}
                  {(data.varpct === undefined || data.varpct === null) ? '' : (data.varpct < 0) ? <span style={{color:'red'}}>{data.varpct}</span> : <span style={{color:'green'}}>{data.varpct}</span>}
                </Grid>

              </Grid>
            </form>
  
          <Snackbar open={error} autoHideDuration={2000} onClose={handleError}>
              <Alert onClose={handleError} severity="error">
                Digite um ativo válido.
              </Alert>
            </Snackbar>
            
          </Container>
      </ThemeProvider>
    </>
    )
}

export default Bolsa;