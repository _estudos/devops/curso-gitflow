// Default
import React, { useState }  from 'react';

// Components
import { 
  Drawer,
  AppBar,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  ListItem,
  ListItemText,
  ListItemIcon,
  Collapse,
} from '@material-ui/core'

// Icons
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import PersonIcon from '@material-ui/icons/Person';
import SearchIcon from '@material-ui/icons/Search';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { FaReact, FaSpider} from 'react-icons/fa';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

// Estilo
import clsx from 'clsx';
import homeStyles from './styles';
import Particles from '../../components/Particles';

// Internos
import Github from '../Github';
import Bolsa from '../Bolsa';
import Nasa from '../Nasa';

// import IMDB from '../Crawler/imdb/'
import MOEDAS from '../Crawler/moedas/'
import Sobre from '../Sobre/'
import Chart from '../Charts'
import { createMuiTheme , ThemeProvider } from '@material-ui/core/styles';



const Home = () => {

  // Altera tema MaterialUI
  const theme = createMuiTheme({
    typography: {
      fontFamily: [
        '-apple-system',
        'Roboto',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
    ].join(','), 
    h2:  {
      fontSize: '1.7rem'
    },
    fontSize: 12
  },
  });

  // Estilos MaterialUI
  const classes = homeStyles(theme);

  // States
  const [subtitle, setSubtitle] = useState('My react training lab')
  const [open, setOpen] = useState(false);
  const [view, setView] = useState('');
  const [openCraw, setOpenCraw] = useState(false)
  const [openApi, setOpenApi] = useState(false)

  // Handles
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleNestCrawler = () => {
    setOpenCraw(!openCraw)
  }
  
  const handleNestAPI = () => {
    setOpenApi(!openApi)
  }
  
  const handleConsultas= (id) => {
    switch(id){
      case 1: 
        setView(<Github/>)
        setSubtitle("Github")
        setOpen(false);
      break;
      case 2:
        setView(<Bolsa/>)
        setSubtitle("Ativos da Bolsa")
        setOpen(false);
      break;
      case 3:
        setView(<Nasa/>)
        setSubtitle('Foto do dia - Nasa');
        setOpen(false);
      break;
      case 5:
        setView(<MOEDAS/>)
        setSubtitle("WebCrawler Moedas");
        setOpen(false);
      break;
      case 6:
        setView(<Chart/>)
        setSubtitle('Charts');
        setOpen(false);
        break;
      default:
        setView(<Sobre/>)
        setSubtitle('Sobre mim');
        setOpen(false);
      break;
    }
  }
  
  return (
    <>
    <CssBaseline />
    <ThemeProvider theme={theme}>
      <div className={classes.root}>

          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: open,
            })}
          >
            <Toolbar >
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
                <Typography variant="h2" noWrap>
                  {(subtitle === 'My react training lab') ? <FaReact/> : ''} {subtitle}
                </Typography>
            </Toolbar>

          </AppBar>

          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
              paper: classes.drawerPaper,
            }}
          >

          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>

          <Divider variant="middle"/>
            <List>
              <ListItem button onClick={handleNestAPI}>
                  <ListItemIcon>
                    {<SearchIcon/>}
                  </ListItemIcon>
                  <ListItemText primary={<strong>Consumo de API</strong>} />
                  {openApi ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openApi} timeout="auto" unmountOnExit>

                  <List disablePadding >
                    
                    <ListItem button onClick={() => handleConsultas(1)} style={{display: 'flex', textAlign: 'center'}}>
                      <ListItemText primary="GitHub"/>
                    </ListItem>
                  </List>
                  <List disablePadding >
                    <ListItem button onClick={() => handleConsultas(2)} style={{display: 'flex', textAlign: 'center'}}>
                      <ListItemText primary="Bolsa"/>
                    </ListItem>
                  </List>
                  <List disablePadding >
                    <ListItem button onClick={() => handleConsultas(3)} style={{display: 'flex', textAlign: 'center'}}>
                      <ListItemText primary="Nasa"/>
                    </ListItem>
                  </List>
              </Collapse>
              
              <ListItem button onClick={handleNestCrawler}>
                <ListItemIcon>
                  {<FaSpider size='22'/>}
                </ListItemIcon>
                <ListItemText primary={<strong>Crawler</strong>} />
                {openCraw ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={openCraw} timeout="auto" unmountOnExit>
                <List disablePadding >
                  <ListItem button onClick={() => handleConsultas(5)} style={{display: 'flex', textAlign: 'center'}}>
                    <ListItemText primary="Moedas"/>
                  </ListItem>
                </List>
              </Collapse>

              <ListItem button key={'6'} onClick={() => handleConsultas(6)}>
                <ListItemIcon>{<AssignmentIcon/>}</ListItemIcon>
                <ListItemText primary={<strong>Charts</strong>} />
              </ListItem>
          </List>

          <Divider variant='middle'/>
          
          <List>
            <ListItem button key={'7'} onClick={() => handleConsultas(7)}>
              <ListItemIcon>{<PersonIcon/>}</ListItemIcon>
              <ListItemText primary={<strong>Sobre mim</strong>} />
            </ListItem>
          </List>

        </Drawer>
          <main className={clsx(classes.content, {
              [classes.contentShift]: open,
            })}>
            <div className={classes.drawerHeader}/>
            {view}
            <Particles/>
          </main>
      </div>
    </ThemeProvider>
    </>
  );
}

export default Home;