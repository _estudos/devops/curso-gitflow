import React , {useState, useEffect} from 'react';
import axios from 'axios';
import clsx from 'clsx';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import nasaLogo from '../../assets/images/nasa-logo.png'
import nasaStyles from './styles';

import {
    Card,
    CardHeader,
    Container,
    CardMedia,
    CardContent,
    CardActions,
    Collapse,
    Avatar,
    IconButton,
    Typography,
}   
from '@material-ui/core';


const Nasa = () => {

    const classes = nasaStyles();
    const [data, setData] = useState([]);
    const [expanded, setExpanded] = React.useState(false);
    const url = 'https://api.nasa.gov/planetary/apod?api_key=YtdotVmYqalMwzoTtq08n5LUNaIxlZsCF0jSkMKq'
    

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    useEffect(() => {
        (() => {
            axios.get(url)
                .then(res => {
                    setData(res.data);
                })        
        })()
    }, [])

    return (
        <>
            <Container style={{display:'flex' ,alignContent:'center'}}>
                <Card className={classes.root}>
                    <CardHeader
                        avatar={
                            <Avatar aria-label="recipe" className={classes.avatar} src={nasaLogo}></Avatar>
                        }
                        action={
                            <IconButton aria-label="settings">
                            </IconButton>
                        }
                        title={data.title}
                        subheader={data.date}
                        />
                    <CardMedia
                        className={classes.media}
                        image={`${data.hdurl}`}
                        title={`${data.title}`}
                    />
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {data.title}
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton
                            className={clsx(classes.expand, {
                                [classes.expandOpen]: expanded,
                            })}
                            onClick={handleExpandClick}
                            aria-expanded={expanded}
                            >
                            {(expanded === true) ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/> }
                        </IconButton>   
                    </CardActions>
                    <Collapse in={expanded} timeout="auto" unmountOnExit>
                        <Typography paragraph style={{padding:"15px", textAlign:"justify"}}>
                            {data.explanation}
                        </Typography>   
                    </Collapse>

                </Card>
         </Container>
        
        </>
    )
}

export default Nasa;