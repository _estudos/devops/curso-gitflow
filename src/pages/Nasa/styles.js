import { makeStyles } from '@material-ui/core/styles';


const nasaStyles = makeStyles((theme) => ({
    root: {
      minWidth: '100%',
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      // transform: 'rotate(180deg)',
    },
}));

export default nasaStyles;