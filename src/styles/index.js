import { makeStyles } from '@material-ui/core/styles';

const globalStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '5px',
    backgroundColor: 'white'
  },
  gridTitle: {
    padding: '10px',
    textAlign: 'center',
  },
  gridTitleTypo: {
    padding: '10px',
    textAlign: 'center',
  },
  gridTitleField: {
    textAlign: 'center',
  },
  containerData: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    marginBottom: '10px',
  },
  gridData: {
    padding: '8px',
  },
  img: {
    borderRadius: '10px',
    width: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      width: "60px",
    },
    [theme.breakpoints.up('md')]: {
      width: "80px",
    },
    [theme.breakpoints.up('lg')]: {
      width: '150px',
    },
  },
  
    
}));

export default globalStyles;