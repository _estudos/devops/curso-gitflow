import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import NotFound from './pages/NotFound';
import Bolsa from './pages/Bolsa';
import Nasa from './pages/Nasa';
import Github from './pages/Github';
import Chart from './pages/Charts'

function App() {
  return (
   <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={Home}  />
      <Route path="/bolsa" component={Bolsa} />
      <Route path="/github" component={Github} />
      <Route path="/nasa" component={Nasa} />
      <Route path="/chart" component={Chart} />
      <Route path="*" component={NotFound} />
    </Switch>
   </BrowserRouter>
  );
}

export default App;
