import Particles from 'react-particles-js';
import React from 'react';

const particles = () => {

    return (
        <Particles
        params={{
            particles: {
                number: {
                value: 5,
                density: {
                    enable: true,
                    value_area: 30
                }
                }
            },
            interactivity: {
                events: {
                    onhover: {
                        enable: true,
                        mode: "repulse"
                    }
                }
            },
            opacity: {
                value: 0.5
            }
        
            }} 
        />
    )
}

export default particles;