import React from 'react';
import {
    ExpansionPanelDetails,
    Typography
} from '@material-ui/core'


const StyledExpansion = (props) => {
    return(
        <ExpansionPanelDetails>
            <Typography style={{fontWeight: "bold"}}>
                {props.description }
            </Typography>
            <Typography>
                {props.data}
            </Typography>
        </ExpansionPanelDetails>
    )
}

export default StyledExpansion;