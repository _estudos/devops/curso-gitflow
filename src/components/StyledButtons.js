import React from 'react';
import {
    Fab
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';


function SearchButton(props) {

    return(
        <>
            <Fab
                size="small"
                type="submit"
                style={{ background: "#003366",marginLeft:'10px'}}
                onClick={props.onClick}
                >
                <SearchIcon style={{ color: "#fff" }}/>
            </Fab>
        </>
    )
}

export default SearchButton